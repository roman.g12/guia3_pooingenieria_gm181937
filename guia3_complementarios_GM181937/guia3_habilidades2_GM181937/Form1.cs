using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace guia3_habilidades2_GM181937
{
    public partial class Form1 : Form
    {
        string pattern = @"\d+";
        int cantidadEstudiantes = 0;
        int c = 0;
        List<Alumno> estudiantes = new List<Alumno>();

        public Form1()
        {
            InitializeComponent();
        }

        public void LimpiarControles()
        {
            txtApellidos.Clear();
            txtCarnet.Clear();
            txtMateria.Clear();
            txtNombres.Clear();
            txtNota1.Clear();
            txtNota2.Clear();
            txtNota3.Clear();
        }

        private void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            //Evaluamos con un regex para que solo sean dígitos los que ingrese en el texbox
            Regex rxSoloNumeros = new Regex(pattern);
            if (rxSoloNumeros.IsMatch(txtCantidad.Text))
            {

            }
            else
            {
                txtCantidad.Text = ""; //Si lo que se ingresa no es un dígito limpiamos la TextBox
            }
        }

        private void btnCantidad_Click(object sender, EventArgs e)
        {
            try
            {
                cantidadEstudiantes = int.Parse(txtCantidad.Text);
                MessageBox.Show("Cantidad de estudiantes ingresada", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCantidad.Enabled = false;
                btnCantidad.Enabled = true;

                txtCarnet.Enabled = true;
                txtNombres.Enabled = true;
                txtApellidos.Enabled = true;
                txtMateria.Enabled = true;
                txtNota1.Enabled = true;
                txtNota2.Enabled = true;
                txtNota3.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Ingrese una cantidad de usuarios valida", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            Alumno alumno = new Alumno();
            if (c < cantidadEstudiantes)
            {
                //MessageBox.Show("entre al if");
                try
                {
                    alumno.Nombre = txtNombres.Text;
                    alumno.Apellido = txtApellidos.Text;
                    alumno.Carnet = txtCarnet.Text;
                    alumno.Materia = txtMateria.Text;
                    alumno.Calificaciones[0] = double.Parse(txtNota1.Text);
                    alumno.Calificaciones[1] = double.Parse(txtNota2.Text);
                    alumno.Calificaciones[2] = double.Parse(txtNota3.Text);
                    estudiantes.Add(alumno);
                    LimpiarControles();
                    c++;

                    if (c == cantidadEstudiantes)
                    {
                        txtCarnet.Enabled = false;
                        txtNombres.Enabled = false;
                        txtApellidos.Enabled = false;
                        txtMateria.Enabled = false;
                        txtNota1.Enabled = false;
                        txtNota2.Enabled = false;
                        txtNota3.Enabled = false;

                        btnIngresar.Enabled = false;
                        btnMostrar.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            dgvEstudiantes.Rows.Clear();
            foreach (Alumno a in estudiantes)
            {
                int index = dgvEstudiantes.Rows.Add();
                dgvEstudiantes.Rows[index].Cells[0].Value = a.Carnet;
                dgvEstudiantes.Rows[index].Cells[1].Value = a.Nombre;
                dgvEstudiantes.Rows[index].Cells[2].Value = a.Apellido;
                dgvEstudiantes.Rows[index].Cells[3].Value = a.Materia;
                dgvEstudiantes.Rows[index].Cells[4].Value = a.Calificaciones[0];
                dgvEstudiantes.Rows[index].Cells[5].Value = a.Calificaciones[1];
                dgvEstudiantes.Rows[index].Cells[6].Value = a.Calificaciones[2];
            }
        }
    }
}
