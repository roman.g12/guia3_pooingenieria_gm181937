﻿
namespace guia3_habilidades2_GM181937
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIngresarEstudiantes = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.btnCantidad = new System.Windows.Forms.Button();
            this.lblIngresarInformacion = new System.Windows.Forms.Label();
            this.grbInformacionEstudiantes = new System.Windows.Forms.GroupBox();
            this.txtMateria = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtCarnet = new System.Windows.Forms.TextBox();
            this.lblMateria = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblNombres = new System.Windows.Forms.Label();
            this.lblCarnet = new System.Windows.Forms.Label();
            this.grbNotas = new System.Windows.Forms.GroupBox();
            this.txtNota3 = new System.Windows.Forms.TextBox();
            this.txtNota2 = new System.Windows.Forms.TextBox();
            this.txtNota1 = new System.Windows.Forms.TextBox();
            this.lblNota3 = new System.Windows.Forms.Label();
            this.lblNota2 = new System.Windows.Forms.Label();
            this.lblNota1 = new System.Windows.Forms.Label();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.grbEstudiantes = new System.Windows.Forms.GroupBox();
            this.dgvEstudiantes = new System.Windows.Forms.DataGridView();
            this.Carnet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Materia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nota1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nota2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nota3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grbInformacionEstudiantes.SuspendLayout();
            this.grbNotas.SuspendLayout();
            this.grbEstudiantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiantes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIngresarEstudiantes
            // 
            this.lblIngresarEstudiantes.AutoSize = true;
            this.lblIngresarEstudiantes.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIngresarEstudiantes.Location = new System.Drawing.Point(12, 39);
            this.lblIngresarEstudiantes.Name = "lblIngresarEstudiantes";
            this.lblIngresarEstudiantes.Size = new System.Drawing.Size(257, 17);
            this.lblIngresarEstudiantes.TabIndex = 0;
            this.lblIngresarEstudiantes.Text = "Ingrese la cantidad de estudiantes:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(275, 38);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(80, 23);
            this.txtCantidad.TabIndex = 1;
            this.txtCantidad.TextChanged += new System.EventHandler(this.txtCantidad_TextChanged);
            // 
            // btnCantidad
            // 
            this.btnCantidad.Location = new System.Drawing.Point(377, 28);
            this.btnCantidad.Name = "btnCantidad";
            this.btnCantidad.Size = new System.Drawing.Size(75, 41);
            this.btnCantidad.TabIndex = 2;
            this.btnCantidad.Text = "Ingresar";
            this.btnCantidad.UseVisualStyleBackColor = true;
            this.btnCantidad.Click += new System.EventHandler(this.btnCantidad_Click);
            // 
            // lblIngresarInformacion
            // 
            this.lblIngresarInformacion.AutoSize = true;
            this.lblIngresarInformacion.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIngresarInformacion.Location = new System.Drawing.Point(97, 100);
            this.lblIngresarInformacion.Name = "lblIngresarInformacion";
            this.lblIngresarInformacion.Size = new System.Drawing.Size(303, 17);
            this.lblIngresarInformacion.TabIndex = 3;
            this.lblIngresarInformacion.Text = "Ingrese la información de los estudiantes";
            // 
            // grbInformacionEstudiantes
            // 
            this.grbInformacionEstudiantes.Controls.Add(this.txtMateria);
            this.grbInformacionEstudiantes.Controls.Add(this.txtApellidos);
            this.grbInformacionEstudiantes.Controls.Add(this.txtNombres);
            this.grbInformacionEstudiantes.Controls.Add(this.txtCarnet);
            this.grbInformacionEstudiantes.Controls.Add(this.lblMateria);
            this.grbInformacionEstudiantes.Controls.Add(this.lblApellidos);
            this.grbInformacionEstudiantes.Controls.Add(this.lblNombres);
            this.grbInformacionEstudiantes.Controls.Add(this.lblCarnet);
            this.grbInformacionEstudiantes.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.grbInformacionEstudiantes.Location = new System.Drawing.Point(12, 141);
            this.grbInformacionEstudiantes.Name = "grbInformacionEstudiantes";
            this.grbInformacionEstudiantes.Size = new System.Drawing.Size(440, 162);
            this.grbInformacionEstudiantes.TabIndex = 4;
            this.grbInformacionEstudiantes.TabStop = false;
            this.grbInformacionEstudiantes.Text = "Datos del estudiante";
            // 
            // txtMateria
            // 
            this.txtMateria.Enabled = false;
            this.txtMateria.Location = new System.Drawing.Point(141, 125);
            this.txtMateria.Name = "txtMateria";
            this.txtMateria.Size = new System.Drawing.Size(269, 25);
            this.txtMateria.TabIndex = 5;
            // 
            // txtApellidos
            // 
            this.txtApellidos.Enabled = false;
            this.txtApellidos.Location = new System.Drawing.Point(141, 93);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(269, 25);
            this.txtApellidos.TabIndex = 5;
            // 
            // txtNombres
            // 
            this.txtNombres.Enabled = false;
            this.txtNombres.Location = new System.Drawing.Point(141, 57);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(269, 25);
            this.txtNombres.TabIndex = 5;
            // 
            // txtCarnet
            // 
            this.txtCarnet.Enabled = false;
            this.txtCarnet.Location = new System.Drawing.Point(141, 27);
            this.txtCarnet.Name = "txtCarnet";
            this.txtCarnet.Size = new System.Drawing.Size(269, 25);
            this.txtCarnet.TabIndex = 4;
            // 
            // lblMateria
            // 
            this.lblMateria.AutoSize = true;
            this.lblMateria.Location = new System.Drawing.Point(37, 125);
            this.lblMateria.Name = "lblMateria";
            this.lblMateria.Size = new System.Drawing.Size(65, 17);
            this.lblMateria.TabIndex = 3;
            this.lblMateria.Text = "Materia:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Location = new System.Drawing.Point(37, 93);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(77, 17);
            this.lblApellidos.TabIndex = 2;
            this.lblApellidos.Text = "Apellidos:";
            // 
            // lblNombres
            // 
            this.lblNombres.AutoSize = true;
            this.lblNombres.Location = new System.Drawing.Point(37, 60);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(77, 17);
            this.lblNombres.TabIndex = 1;
            this.lblNombres.Text = "Nombres:";
            // 
            // lblCarnet
            // 
            this.lblCarnet.AutoSize = true;
            this.lblCarnet.Location = new System.Drawing.Point(37, 30);
            this.lblCarnet.Name = "lblCarnet";
            this.lblCarnet.Size = new System.Drawing.Size(60, 17);
            this.lblCarnet.TabIndex = 0;
            this.lblCarnet.Text = "Carnet:";
            // 
            // grbNotas
            // 
            this.grbNotas.Controls.Add(this.txtNota3);
            this.grbNotas.Controls.Add(this.txtNota2);
            this.grbNotas.Controls.Add(this.txtNota1);
            this.grbNotas.Controls.Add(this.lblNota3);
            this.grbNotas.Controls.Add(this.lblNota2);
            this.grbNotas.Controls.Add(this.lblNota1);
            this.grbNotas.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.grbNotas.Location = new System.Drawing.Point(12, 322);
            this.grbNotas.Name = "grbNotas";
            this.grbNotas.Size = new System.Drawing.Size(440, 145);
            this.grbNotas.TabIndex = 6;
            this.grbNotas.TabStop = false;
            this.grbNotas.Text = "Notas";
            // 
            // txtNota3
            // 
            this.txtNota3.Enabled = false;
            this.txtNota3.Location = new System.Drawing.Point(141, 98);
            this.txtNota3.Name = "txtNota3";
            this.txtNota3.Size = new System.Drawing.Size(159, 25);
            this.txtNota3.TabIndex = 5;
            // 
            // txtNota2
            // 
            this.txtNota2.Enabled = false;
            this.txtNota2.Location = new System.Drawing.Point(141, 62);
            this.txtNota2.Name = "txtNota2";
            this.txtNota2.Size = new System.Drawing.Size(159, 25);
            this.txtNota2.TabIndex = 5;
            // 
            // txtNota1
            // 
            this.txtNota1.Enabled = false;
            this.txtNota1.Location = new System.Drawing.Point(141, 27);
            this.txtNota1.Name = "txtNota1";
            this.txtNota1.Size = new System.Drawing.Size(159, 25);
            this.txtNota1.TabIndex = 4;
            // 
            // lblNota3
            // 
            this.lblNota3.AutoSize = true;
            this.lblNota3.Location = new System.Drawing.Point(37, 101);
            this.lblNota3.Name = "lblNota3";
            this.lblNota3.Size = new System.Drawing.Size(60, 17);
            this.lblNota3.TabIndex = 2;
            this.lblNota3.Text = "Nota 3:";
            // 
            // lblNota2
            // 
            this.lblNota2.AutoSize = true;
            this.lblNota2.Location = new System.Drawing.Point(37, 65);
            this.lblNota2.Name = "lblNota2";
            this.lblNota2.Size = new System.Drawing.Size(60, 17);
            this.lblNota2.TabIndex = 1;
            this.lblNota2.Text = "Nota 2:";
            // 
            // lblNota1
            // 
            this.lblNota1.AutoSize = true;
            this.lblNota1.Location = new System.Drawing.Point(37, 30);
            this.lblNota1.Name = "lblNota1";
            this.lblNota1.Size = new System.Drawing.Size(60, 17);
            this.lblNota1.TabIndex = 0;
            this.lblNota1.Text = "Nota 1:";
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(12, 483);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(182, 56);
            this.btnIngresar.TabIndex = 7;
            this.btnIngresar.Text = "Ingresar estudiante";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(270, 483);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(182, 56);
            this.btnMostrar.TabIndex = 8;
            this.btnMostrar.Text = "Mostrar estudiante";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // grbEstudiantes
            // 
            this.grbEstudiantes.Controls.Add(this.dgvEstudiantes);
            this.grbEstudiantes.Font = new System.Drawing.Font("Britannic Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.grbEstudiantes.Location = new System.Drawing.Point(478, 28);
            this.grbEstudiantes.Name = "grbEstudiantes";
            this.grbEstudiantes.Size = new System.Drawing.Size(570, 511);
            this.grbEstudiantes.TabIndex = 9;
            this.grbEstudiantes.TabStop = false;
            this.grbEstudiantes.Text = "Estudiantes";
            // 
            // dgvEstudiantes
            // 
            this.dgvEstudiantes.AllowUserToAddRows = false;
            this.dgvEstudiantes.AllowUserToDeleteRows = false;
            this.dgvEstudiantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstudiantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Carnet,
            this.Nombres,
            this.Apellidos,
            this.Materia,
            this.Nota1,
            this.Nota2,
            this.Nota3});
            this.dgvEstudiantes.Location = new System.Drawing.Point(6, 21);
            this.dgvEstudiantes.Name = "dgvEstudiantes";
            this.dgvEstudiantes.ReadOnly = true;
            this.dgvEstudiantes.RowTemplate.Height = 25;
            this.dgvEstudiantes.Size = new System.Drawing.Size(558, 484);
            this.dgvEstudiantes.TabIndex = 0;
            // 
            // Carnet
            // 
            this.Carnet.HeaderText = "Carnet";
            this.Carnet.Name = "Carnet";
            this.Carnet.ReadOnly = true;
            // 
            // Nombres
            // 
            this.Nombres.HeaderText = "Nombres";
            this.Nombres.Name = "Nombres";
            this.Nombres.ReadOnly = true;
            // 
            // Apellidos
            // 
            this.Apellidos.HeaderText = "Apellidos";
            this.Apellidos.Name = "Apellidos";
            this.Apellidos.ReadOnly = true;
            // 
            // Materia
            // 
            this.Materia.HeaderText = "Materia";
            this.Materia.Name = "Materia";
            this.Materia.ReadOnly = true;
            // 
            // Nota1
            // 
            this.Nota1.HeaderText = "Nota1";
            this.Nota1.Name = "Nota1";
            this.Nota1.ReadOnly = true;
            // 
            // Nota2
            // 
            this.Nota2.HeaderText = "Nota2";
            this.Nota2.Name = "Nota2";
            this.Nota2.ReadOnly = true;
            // 
            // Nota3
            // 
            this.Nota3.HeaderText = "Nota3";
            this.Nota3.Name = "Nota3";
            this.Nota3.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 560);
            this.Controls.Add(this.grbEstudiantes);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.grbNotas);
            this.Controls.Add(this.grbInformacionEstudiantes);
            this.Controls.Add(this.lblIngresarInformacion);
            this.Controls.Add(this.btnCantidad);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.lblIngresarEstudiantes);
            this.Name = "Form1";
            this.Text = "Form1";
            this.grbInformacionEstudiantes.ResumeLayout(false);
            this.grbInformacionEstudiantes.PerformLayout();
            this.grbNotas.ResumeLayout(false);
            this.grbNotas.PerformLayout();
            this.grbEstudiantes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIngresarEstudiantes;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Button btnCantidad;
        private System.Windows.Forms.Label lblIngresarInformacion;
        private System.Windows.Forms.GroupBox grbInformacionEstudiantes;
        private System.Windows.Forms.TextBox txtMateria;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtCarnet;
        private System.Windows.Forms.Label lblMateria;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label lblNombres;
        private System.Windows.Forms.Label lblCarnet;
        private System.Windows.Forms.GroupBox grbNotas;
        private System.Windows.Forms.TextBox txtNota3;
        private System.Windows.Forms.TextBox txtNota2;
        private System.Windows.Forms.TextBox txtNota1;
        private System.Windows.Forms.Label lblNota3;
        private System.Windows.Forms.Label lblNota2;
        private System.Windows.Forms.Label lblNota1;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.GroupBox grbEstudiantes;
        private System.Windows.Forms.DataGridView dgvEstudiantes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Carnet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombres;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Materia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nota1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nota2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nota3;
    }
}

