﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia3_habilidades2_GM181937
{
    class Alumno
    {
        string carnet;
        public string Carnet
        {
            get { return carnet; }
            set { carnet = value; }
        }
        string nombre;
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        string apellido;
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        string materia;
        public string Materia
        {
            get { return materia; }
            set { materia = value; }
        }
        double[] calificaciones = new double[3];
        public double[] Calificaciones
        {
            get { return calificaciones; }
            set { calificaciones = value; }
        }
    }
}
