﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace guia3_habilidades3_GM181937
{
    public partial class Form1 : Form
    {
        /*listado que permite tener varios elementos de la clase Persona*/
        private List<Producto> Productos = new List<Producto>();
        private int edit_indice = -1; /*el índice para editar comienza en -1, esto significa que
        no hay ninguno seleccionado, esto servirá para el DataGridView.*/

        public Form1()
        {
            InitializeComponent();
        }

        private void actualizarGrid()
        {
            dgvlistado.DataSource = null;
            dgvlistado.DataSource = Productos; /*los nombres de columna que veremos son los de las propiedades*/
        }

        private void reseteo()
        {
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtMarca.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
        }

        string pathImage = null;
        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            int size = -1;
            DialogResult result = ofdImagen.ShowDialog();
            ofdImagen.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (result == DialogResult.OK)
            {
                pathImage = ofdImagen.FileName;
                try
                {
                    string text = File.ReadAllText(pathImage);
                    size = text.Length;
                }
                catch (IOException)
                {
                    MessageBox.Show("Se ha producido un error");
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Producto producto = new Producto();
            //creo un objeto de la clase persona y guardo a través de las propiedades
            Producto product = new Producto();
            product.Nombre = txtNombre.Text;
            product.Descripcion = txtDescripcion.Text;
            product.Marca = txtMarca.Text;
            product.Precio = float.Parse(txtPrecio.Text);
            product.Stock = int.Parse(txtStock.Text);
            product.Imagen = pathImage;
            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Productos[edit_indice] = product;
                edit_indice = -1;
            }
            else
            {
                Productos.Add(product); /*al arreglo de Productos le agrego el objeto creado con todos los datos que recolecté*/
            }
            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            reseteo(); //llamamos al método que resetea
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Productos.RemoveAt(edit_indice);
                edit_indice = -1; //resetea variable a -1
                reseteo();
                actualizarGrid();
            }
            else
            {
                MessageBox.Show("Dar doble click sobre elemento para seleccionar y borrar ");
            }
        }

        private void dgvlistado_DoubleClick(object sender, EventArgs e)
        {
            Bitmap mapaImagenProducto;
            DataGridViewRow selected = dgvlistado.SelectedRows[0];
            int posicion = dgvlistado.Rows.IndexOf(selected); //almacena en cual fila estoy
            edit_indice = posicion; //copio esa variable en índice editado
            Producto product = Productos[posicion]; /*esta variable de tipo persona, se carga con los valores que le pasa el listado*/
            //lo que tiene el atributo se lo doy al textbox
            mapaImagenProducto = new Bitmap(product.Imagen);
            txtNombre.Text = product.Nombre;
            txtDescripcion.Text = product.Descripcion;
            txtMarca.Text = product.Marca;
            txtPrecio.Text = Convert.ToString(product.Precio);
            txtStock.Text = Convert.ToString(product.Stock);
            pbImagen.Image = (Image)mapaImagenProducto;
        }
    }
}
