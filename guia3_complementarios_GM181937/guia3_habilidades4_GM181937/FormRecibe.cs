﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace guia3_habilidades4_GM181937
{
    public partial class FormRecibe : Form
    {
        public FormRecibe()
        {
            InitializeComponent();
        }

        public List<Persona> PersonaRecibe = null; //creación de una lista que reciba
        private void actualizarGrid() //función que llena el DGV del formulario 2
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = PersonaRecibe;
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            actualizarGrid();
        }
    }
}
