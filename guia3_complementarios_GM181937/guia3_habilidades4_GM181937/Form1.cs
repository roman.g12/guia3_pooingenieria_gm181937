﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guia3_habilidades4_GM181937
{
    public partial class Form1 : Form
    {
        private List<Persona> Personas = new List<Persona>();
        private List<Persona> PersonasFiltrado = new List<Persona>();
        private int edit_indice = -1;
        private int filtro_indice = -1;
        private bool bandera; //bandera que nos ayudara a saber cuando limpiar la lista de filtro

        public Form1()
        {
            InitializeComponent();
        }

        private void actualizarGrid()
        {
            dgvContactos.DataSource = null;
            dgvContactos.DataSource = Personas; /*los nombres de columna que veremos son los de las propiedades*/
        }

        private void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtTelefono.Clear();
            txtCorreo.Clear();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //creo un objeto de la clase persona y guardo a través de las propiedades
            Persona per = new Persona();
            per.Nombre = txtNombre.Text;
            per.Apellido = txtApellido.Text;
            per.Telefono = txtTelefono.Text;
            per.Correo = txtCorreo.Text;
            if (edit_indice > -1) //verifica si hay un índice seleccionado 0
            {
                Personas[edit_indice] = per;
                edit_indice = -1;
            }
            else
            {
                Personas.Add(per); /*al arreglo de Personas le agrego el objeto creado con todos los datos que recolecté*/
            }

            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            limpiar();//mandamos a llamar la función que limpia
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Personas.RemoveAt(edit_indice);
                edit_indice = -1; //resetea variable a -1
                limpiar();
                actualizarGrid();
            }
            else
            {
                MessageBox.Show("Debe dar doble click primero sobre contacto");
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            FormRecibe formulario = new FormRecibe(); //instancia de otro formulario
            if (bandera == true)
            {
                //Limpiamos el filtro para si se quiere enviar toda la lista sin filtro
                PersonasFiltrado.RemoveRange(0, PersonasFiltrado.Count);
            }
            if (PersonasFiltrado.Count > 0)
            {
                formulario.PersonaRecibe = PersonasFiltrado;
                formulario.Show(); //mostar el segundo formulario
                MessageBox.Show("Enviando contactos seleccionados");

                bandera = true;
            }
            else
            {
                formulario.PersonaRecibe = Personas; /*lista original Personas es enviada a la lista PersonaRecibe que está en el formulario 2 y que puede ser invocada por medio de la instancia del segundo formulario */
                formulario.Show(); //mostar el segundo formulario
            }
        }

        private void dgvContactos_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dgvContactos.SelectedRows[0];
            int pos = dgvContactos.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            edit_indice = pos; //copio esa variable en índice editado
            Persona per = Personas[pos]; /*esta variable de tipo persona, se carga con los valores que le pasa el listado*/
            txtNombre.Text = per.Nombre; //lo que tiene el atributo se lo doy al textbox
            txtApellido.Text = per.Apellido;
            txtTelefono.Text = per.Telefono;
            txtCorreo.Text = per.Correo;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (filtro_indice > -1) //verifica si hay un índice seleccionado
            {
                PersonasFiltrado.Add(Personas[filtro_indice]);
                filtro_indice = -1;
                limpiar();
                actualizarGrid();
                MessageBox.Show("Se agrego el contacto al filtro");
                bandera = false;
            }
            else
            {
                MessageBox.Show("Debe dar click primero sobre contacto");
            }
        }

        private void dgvContactos_Click(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dgvContactos.SelectedRows[0];
            int pos = dgvContactos.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            filtro_indice = pos; //copio esa variable en índice de filtrado
            Persona per = Personas[pos]; /*esta variable de tipo persona, se carga con los 
                                          * valores que le pasa el listado*/
        }
    }
}
