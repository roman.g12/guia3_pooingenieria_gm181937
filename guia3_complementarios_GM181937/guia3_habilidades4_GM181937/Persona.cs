﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia3_habilidades4_GM181937
{
    public class Persona
    {
        private string nombre;
        private string apellido;
        private string telefono;
        private string correo;

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Correo { get => correo; set => correo = value; }
    }
}
