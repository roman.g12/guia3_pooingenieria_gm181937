﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia3_habilidades1_GM181937
{
    class Clientes
    {
        string dui;
        public string Dui
        {
            get { return dui; }
            set { dui = value; }
        }

        string nit;
        public string Nit
        {
            get { return nit; }
            set { nit = value; }
        }

        string nombre;
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        string apellido;
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        string tipoCuenta;
        public string TipoCuenta
        {
            get { return tipoCuenta; }
            set { tipoCuenta = value; }
        }

        string numneroCuenta;
        public string NumneroCuenta
        {
            get { return numneroCuenta; }
            set { numneroCuenta = value; }
        }

        float monto;
        public float Monto
        {
            get { return monto; }
            set { monto = value; }
        }

        string sucursal;
        public string Sucursal
        {
            get { return sucursal; }
            set { sucursal = value; }
        }
    }
}
