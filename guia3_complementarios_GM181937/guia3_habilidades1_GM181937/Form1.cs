﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guia3_habilidades1_GM181937
{
    public partial class Form1 : Form
    {
        private List<Clientes> Personas = new List<Clientes>();
        private List<Clientes> PersonasFiltrado = new List<Clientes>();
        private int edit_indice = -1;
        public Form1()
        {
            InitializeComponent();
        }

        private void actualizarGrid()
        {
            dgvDatos.DataSource = null;
            dgvDatos.DataSource = Personas; /*los nombres de columna que veremos son los de las propiedades*/
        }

        private void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDUI.Clear();
            txtNIT.Clear();
            txtMonto.Clear();
            txtNumeroCuenta.Clear();
            cbSucursal.Text = "Seleccionar";
            cbTipoCuenta.Text = "Seleccionar";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            //Creamos objeto de la clase cliente
            Clientes cliente = new Clientes();
            //Validamos que no quede ningún campo vacío
            if (txtDUI.Text != "" && txtNIT.Text != "" && txtNombre.Text != "" && txtApellido.Text != "" && txtMonto.Text != "")
            {
                if (cbTipoCuenta.Text != "Seleccionar" && cbSucursal.Text != "Seleccionar")
                {
                    cliente.Nombre = txtNombre.Text;
                    cliente.Apellido = txtApellido.Text;
                    cliente.Dui = txtDUI.Text;
                    cliente.Nit = txtNIT.Text;
                    cliente.Sucursal = cbSucursal.Text;
                    cliente.TipoCuenta = cbTipoCuenta.Text;
                    cliente.NumneroCuenta = txtNumeroCuenta.Text;
                    cliente.Monto = float.Parse(txtMonto.Text);
                    if (edit_indice > -1) //verifica si hay un índice seleccionado 0
                    {
                        Personas[edit_indice] = cliente;
                        edit_indice = -1;
                    }
                    else
                    {
                        Personas.Add(cliente); /*al arreglo de Personas le agrego el objeto creado con todos los datos que recolecté*/
                    }

                    actualizarGrid();//llamamos al procedimiento que guarda en datagrid
                    limpiar();//mandamos a llamar la función que limpia
                }
                else
                {
                    MessageBox.Show("Verificar que todos lo campos esten completos");
                }
            }
            else
            {
                MessageBox.Show("No deje campos vacíos");
            }
        }

        private void cbTipoCuenta_SelectedIndexChanged(object sender, EventArgs e)
        {
            Random aleatorio = new Random();
            int numero;
            numero = aleatorio.Next(1000, 9999);

            if (cbTipoCuenta.Text != "Seleccionar")
            {
                if (cbTipoCuenta.Text == "Corriente")
                {
                    txtNumeroCuenta.Text = "CC-" + numero;
                }
                else if (cbTipoCuenta.Text == "Ahorros")
                {
                    txtNumeroCuenta.Text = "CA-" + numero;
                }
                else if (cbTipoCuenta.Text == "Plazos")
                {
                    txtNumeroCuenta.Text = "CP-" + numero;
                }
            }
        }

        private void dgvDatos_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dgvDatos.SelectedRows[0];
            int pos = dgvDatos.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            edit_indice = pos; //copio esa variable en índice editado
            Clientes cliente = Personas[pos]; /*esta variable de tipo persona, se carga con los valores que le pasa el listado*/
            txtNombre.Text = cliente.Nombre; //lo que tiene el atributo se lo doy al textbox
            txtApellido.Text = cliente.Apellido;
            txtDUI.Text = cliente.Dui;
            txtNIT.Text = cliente.Nit;
            txtMonto.Text = cliente.Monto.ToString();
            txtNumeroCuenta.Text = cliente.NumneroCuenta;
            cbTipoCuenta.Text = cliente.TipoCuenta;
            cbSucursal.Text = cliente.Sucursal;
        }

        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            PersonasFiltrado.RemoveRange(0, PersonasFiltrado.Count);
            foreach (Clientes a in Personas)
            {
                if (a.Nombre.Contains(txtFiltrar.Text.ToString()))
                {
                    PersonasFiltrado.Add(a);
                }
            }

            if (txtFiltrar.Text == "")
            {
                actualizarGrid();
            }

            dgvDatos.DataSource = null;
            dgvDatos.DataSource = PersonasFiltrado; /*los nombres de columna que veremos son los de las propiedades*/
        }
    }
}
